const express = require('express');
const router = express.Router();
//crear rutas del servidor

router.get('/',(req, res) => {
    res.render('index'); //redirecciona a index
});

router.get('/about',(req, res) => {
    res.render('abaut');
});

router.get('/xapilocal',(req, res) => {
    res.render('xapilocal');
});

router.get('/movements',(req, res) => {
    res.render('movements');
});

router.get('/agente',(req, res) => {
    res.render('agente');
});

module.exports = router;
