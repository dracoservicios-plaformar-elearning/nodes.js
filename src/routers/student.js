const express = require('express');
const router = express.Router();
const fs = require('fs');
const nodemailer = require("nodemailer");
// constate que llama al modelo de la bd
const Estudent = require('../models/Student');
const Course = require('../models/course');
const User = require('../models/User');
const clases = require('../models/class');
const Activity = require('../models/Activity');
const solutionActivity = require('../models/solutionActivity');
const ActivityXapi = require('../models/ActivityXapi');

// funcion para restringir el acceso por usuario
const {
    isAuthenticated
} = require('../helpers/auth');

// var files = fs.readdirSync('./uploads')

// ruta para agregar nuevo estudiente
router.get('/student/add/:id', isAuthenticated, async (req, res) => {
    const createEstudent = await Course.findById(req.params.id)
    res.render('student/new-student', {
        createEstudent
    })
});


// matricular estudiente
router.post('/student/new-student/', isAuthenticated, async (req, res) => {
    const {
        name,
        email,
        mobile,
        password,
        confirm_password,
        idCurse
    } = req.body;
    var rol = 'Estudent';
    const errors = []
    // mensaje de error en caso de que no escriba nombre de usario
    if (!name) {
        errors.push({
            text: 'Debe ingresar el nombre del Estudiante'
        })
    }
    // mensaje de error en caso de que no escriba email
    if (!email) {
        errors.push({
            text: 'Debe ingresar el nombre de la Actividad'
        })
    }
    // mensaje de error en caso de que el campo de contraseña es vacio
    if (password.length <= 0) {
        errors.push({
            text: 'Debe ingresar una contraseña'
        })
    }

    // mensaje de error en caso de las contraseñas no coincidan
    if (password != confirm_password) {
        errors.push({
            text: 'Las contraseñas no coinciden'
        })
    }
    // mensaje de error en caso de que la contraseñas tengan una longitud menor a 4 caracteres
    if (password.length < 4) {
        errors.push({
            text: 'Debe ingresar una contraseña con mas de 4 caracteres'
        })
    }

    if (errors.length > 0) {
        res.render('student/new-student', {
            errors,
            name,
            email,
            mobile,
            password,
            confirm_password,
            idCurse
        });
    } else {
        const emailStuden = await Estudent.findOne({
            email: email
        })
        // validar si el email exite en la BD
        const emailUser = await User.findOne({
            email: email
        })
        if (emailUser || emailStuden) {
            req.flash('success_msg', 'Ya existe un usuario registrado con el Correo Ingresado')
            res.render('student/new-student')

        } else {
            // Guardar un dato nuevo (agregar datos a la BD)

            const newUser = new User({
                name,
                email,
                mobile,
                password,
                rol,
                idCurse
            })
            // enviar confirmación de matricula al correo electronico
       
              // esta variable guarda los datos que seran enviados por correo electronico
              contentHTML = `
              <h1>Usted fue matriculado en la plataforma educativa ATHENAS ✔👍 </h1>
              <h2>Datos de Ingreso a la plataforma</h2>
              <h3>Usuario: ${email}</h3>
              <h3>Contraseña: ${password}</h3>
              <hr>
              <h3>Nombre: ${name}</h3>
              <h3>Rol: ${rol}</h3>`;
      
      
              emailC = `${email}`;
              // configuración del mi correo electronico
              const transporter = nodemailer.createTransport({
                  host: "smtp.gmail.com",
                  port: 587,
                  secure: false, // true for 465, false for other ports
                  auth: {
                  user: 'dracoservivios@gmail.com',
                  pass: 'yqjmpfdiiihfuago', // generated ethereal password
                  },
              });
      
              // contenido de correo electronico
              const info =  transporter.sendMail({
                  from: '"Registro de docente 👍✔" <dracoservivios@gmail.com>', // sender address
                  to: emailC, // list of receivers
                  subject: "Acaba de registrar un nuevo usuario al sistema", // Subject line
                  text: "Mensaje de confirmación 👨‍💻", // plain text body
                  html: contentHTML, // html body
                });
      

            newUser.password = await newUser.encryptPassword(password)
            newUser.user = req.user.id; //id del adminstrador 
            await newUser.save();

            // mensaje de exito
            req.flash('success_msg', 'registro exitoso')

            // redireccionar a la pagina de curso
            res.redirect('/course');
        }

    }
});


//Ruta para vizualizar los estudiantes matriculados
router.get('/student_list/:id', isAuthenticated, async (req, res) => {
    await User.find({
            idCurse: req.params.id,
            rol: 'Estudent',
        }).sort({
            date: 'desc'
        })
        .then(documentos => {
            const contexto = {
                student: documentos.map(documento => {
                    return {
                        name: documento.name,
                        email: documento.email,
                        mobile: documento.mobile,
                        _id: documento._id,
                        date: documento.date,
                    }
                })
            }
            res.render('student/all-student', {
                student: contexto.student
            })
        })
});

// esta funcion envia los datos del estudiante que quiero editar por medio de su id al formulario
router.get('/student/edit/:id', isAuthenticated, async (req, res) => {
    const student = await User.findById(req.params.id)
    res.render('student/edit-student', {
        student
    })
});



///FUCIONES DEL ESTUDIANTE

// funcion para visualizar las clases de cada curso
router.get('/unidad/:id', isAuthenticated, async (req, res) => {
    
    new_activity = await  Activity.findOne({idCurse: req.user.idCurse})
    .sort({$natural:-1}).limit(1);                     
    hoy = new Date()

    activity_actual = new_activity.start_date
    dia_activity=activity_actual.getDay(); 
    mes_activity=activity_actual.getMonth(); //mes de la inicio de la actividad
    ano_activity=hoy.getYear(); //año actual

    diahoy=hoy.getDate();
    meshoy=activity_actual.getMonth(); //mes actual
    annohoy=hoy.getYear(); //año actual

    start_date = [dia_activity,mes_activity,ano_activity ]
       await clases.find({
            idCurse: req.params.id
        }).sort({
            date: 'desc'
        })
        .then(documentos => {
            const contexto = {
                clases: documentos.map(documento => {
                    return {
                        nameClass: documento.nameClass,
                        description: documento.description,
                        _id: documento._id,
                        date: documento.date,
                        useremail: req.user.email,
                        user: req.user._id
                    }
                })
            }
            res.render('student/unidad-student', {
                clases: contexto.clases,
                new_activity,
                activity_actual,
                start_date
            })
        })
});

// funcion para vusualizar las actividades relacionadas con la clases
router.get('/modulo/:id', isAuthenticated, async (req, res) => {
    const clases2 = await clases.findById(req.params.id)
    await Activity.find({
            idClass: req.params.id
        }).sort({
            date: 'desc'
        })
        .then(documentos => {
            const contexto = {
                activity: documentos.map(documento => {
                    return {
                        nameActivity: documento.nameActivity,
                        description: documento.description,
                        _id: documento._id,
                        date: documento.date,
                        useremail: req.user.email,
                        user: req.user._id
                    }
                })
            }
            res.render('student/activity-student', {
                activity: contexto.activity,
                clases2
            })
        })
});

// ruta para ver detalle de la actividad
router.get('/activity-view/:id', isAuthenticated, async (req, res) => {
    await Activity.find({
            _id: req.params.id
        }).sort({
            date: 'desc'
        })
        .then(documentos => {
            const contexto = {
                activity: documentos.map(documento => {
                    return {
                        nameActivity: documento.nameActivity,
                        description: documento.description,
                        start_date: documento.start_date,
                        end_date: documento.end_date,
                        _id: documento._id,
                        fileActivity: documento.fileActivity,
                        idClass: documento.idClass,
                        useremail: req.user.email,
                        user: req.user._id
                    }
                })
            }
            res.render('student/solve_activity', {
                activity: contexto.activity
            })
        })
});

// guardar respuesta de actividad
router.post('/student/solution_activity/:id', isAuthenticated, async (req, res) => {
    const {
        idActivity,
        nameActivity,
        solution_activity,
        idClass
    } = req.body;
    const errors = [];
    if (!solution_activity) {
        errors.push({
            text: 'Solucion vacia'
        })
    }
    // si los errores son mayor a 0
    if (errors.length > 0) {
        res.render('student/solution_activity/', {
            errors,
            nameActivity,
            solution_activity,
            idClass
        });
    } else {
        // SI TODO SALE BIEN Y SE RECIEBEN LOS DATOS INGRESADOS Y SE GUARDA EN LA BD
        const solution = new solutionActivity({
            idActivity,
            nameActivity,
            solution_activity,
            idClass
        });
        // console.log(newNote);
        solution.email = req.user.email;
        solution.user = req.user.id;
        await solution.save();
        req.flash('success_msg', 'Respuesta enviada')
        res.redirect('/home');

    }
});

// ruta para abrir documento adjunto en actividad
router.get('/student/file/:id', isAuthenticated, async (req, res) => {
    await Activity.find({
        file: req.params.id
    });
    var file = req.params.id;

    var doc = ('../../' + file + '.pdf')
    var xApi = ('../../xApi/' + file + '/res/')

    if (fs.existsSync('src/uploads/' + file + '.pdf')) {
        res.redirect(doc);

    } else {
        res.redirect(xApi);
    }
});

// función para ver detalle del las actividades realizadad por el estudiante seleccionaldo
router.get('/ratings/', isAuthenticated, async (req, res) => {
    resultUser = await User.findById(req.user.id)
    await ActivityXapi.find({
            user: req.user.id
        }).sort({
            date: 'desc'
        })
        .then(documentos => {
            const contexto = {
                ActivityXapi: documentos.map(documento => {
                    return {
                        nomPrueba: documento.qt,
                        time: documento.fut,
                        required_score: documento.tp,
                        score_won: documento.sp,
                        email: documento.email
                    }
                })
            }
            res.render('unidad/follow_up_student', {
                resultUser,
                ActivityXapi: contexto.ActivityXapi
            })
        })

});


module.exports = router;
