const express = require('express');
const multer = require('multer'); //libreria para guardar archivos en server
const nodemailer = require("nodemailer");
const mimeTypes = require('mime-types')
const router = express.Router();
// constate que llama al modelo de la bd
const clases = require('../models/class');
const Activity = require('../models/Activity');
const solutionActivity = require('../models/solutionActivity');
const ActivityXapi = require('../models/ActivityXapi');
const User = require('../models/User');

// funcion para restringir el acceso por usuario
const {
    isAuthenticated
} = require('../helpers/auth');
const moment = require('moment');
// DESCROMPRIMIER ARCHIVO
const decompress = require('decompress');

// ruta para agregar nuevo curso
router.get('/unidad/add', isAuthenticated, (req, res) => {
    res.render('unidad/new-unidad');
});

// ruta para recibir datos INGRESADOS POR EL USUARIO - TENER EN CUENTA QUE LOS RESIVE POR EL METODO POST
router.post('/unidad/new-unidad', isAuthenticated, async (req, res) => {
    const {
        nameClass,
        specific_goal,
        general_objective,
        problem_question,
        Competencies,
        results
    } = req.body;
    const errors = [];
    // se crea condicionales para que los input no traigan valores vacios
    if (!nameClass) {
        errors.push({
            text: 'Debe Ingresar el Nombre del Modulo'
        })
    }
    if (!specific_goal) {
        errors.push({
            text: 'Debe escribir el objetivo especifico'
        })
    }
    if (!general_objective) {
        errors.push({
            text: 'Debe escribir el Objetivo General'
        })
    }

    // si los errores son mayor a 0
    if (errors.length > 0) {
        res.render('unidad/new-unidad', {
            errors,
            nameClass,
            specific_goal,
            general_objective,
            problem_question,
            Competencies,
            results
        });
    } else {
        // SI TODO SALE BIEN Y SE RECIEBEN LOS DATOS INGRESADOS Y SE GUARDA EN LA BD
        const newclass = new clases({
            errors,
            nameClass,
            specific_goal,
            general_objective,
            problem_question,
            Competencies,
            results
        });
        //console.log(newCourse);
        newclass.user = req.user.id;
        newclass.idCurse = req.user.idCurse;
        await newclass.save();
        req.flash('success_msg', 'Unidad agregada exitosamente')
        res.redirect('/home');
    }
});

// con esta funcion podemos ver las clases del usuario al iniciar sesion
router.get('/unidad/', isAuthenticated, async (req, res) => {
    await clases.find({
            user: req.user.id
        }).sort({
            date: 'desc'
        })
        .then(documentos => {
            const contexto = {
                course: documentos.map(documento => {
                    return {
                        nameClass: documento.nameClass,
                        description: documento.description,
                        _id: documento._id,
                        date: documento.date
                    }
                })
            }
            res.render('unidad/all-unidad', {
                course: contexto.course
            })
        })
});


// esta funcion envia la clase que quiero editar por medio de su id al formulario
router.get('/unidad/edit/:id', isAuthenticated, async (req, res) => {
    const clases2 = await clases.findById(req.params.id)
    res.render('unidad/edit-unidad', {
        clases2
    })
});

// esta funcion guarda los datos editados
router.put('/unidad/edit-unidad/:id', isAuthenticated, async (req, res) => {
    const {
        nameClass,
        specific_goal,
        general_objective,
        problem_question,
        Competencies,
        results
    } = req.body;
    await clases.findByIdAndUpdate(req.params.id, {
        nameClass,
        specific_goal,
        general_objective,
        problem_question,
        Competencies,
        results
    });
    req.flash('success_msg', 'Unidad editada correctamente')
    res.redirect('/home')
});

// esta funcion elimina la nota
router.delete('/unidad/delete/:id', isAuthenticated, async (req, res) => {
    await clases.findByIdAndDelete(req.params.id);
    req.flash('remove', 'Unidad')
    res.redirect('/home')
});

// envia los datos de la actividad
router.get('/modulo_new/:id', isAuthenticated, async (req, res) => {
    const clases2 = await clases.findById(req.params.id)
    res.render('activity/new-activity', {
        clases2
    })
});

// esta declaracion es necesaria para optener archivo
const stora = multer.diskStorage({
    destination: './src/uploads/',
    filename: function (req, file, cb) {
        //cb("", file.originalname
        const archivo = mimeTypes.extension(file.mimetype)
        if (archivo == false) {
            cb("", moment().format('YYYY-MM-DD hh-mm') + req.user.id + "." + 'zip')
        } else {
            cb("", moment().format('YYYY-MM-DD hh-mm') + req.user.id + "." + mimeTypes.extension(file.mimetype))
        }
    }
})

const upload = multer({
    storage: stora
})

// ruta para recibir los datos de la actividad se registran los datos en la BD
router.post('/activity/new-activity', isAuthenticated, upload.single('fileActivity'), async (req, res) => {
    // Resivimos los datos del formulario para guardar actividad
    const {
        nameActivity,
        description,
        start_date,
        end_date,
        idClass,
        fileActivity
    } = req.body;
    // los datos se guardan en un una variable
    const newActivity = new Activity({
        nameActivity,
        description,
        start_date,
        end_date,
        idClass,
        fileActivity
    });
    // realizamos consulta para enviar correo
    send_activity = await  User.find({idCurse: req.user.idCurse})
    .then(documentos => {
        const contexto = {
            course: documentos.map(documento => {
                return {
                   email: documento.email,
                }
            })
        } 
        // res.redirect('/home'),{
        //     course: contexto.course
        // };

        emailUser = {course: contexto.course}
     

    })

    // // obtenemos la fecha actual - para asignarle el nombre al archivo
    const fecha = moment().format('YYYY-MM-DD hh-mm')
    const myfileApi = (fecha + '' + req.user.id)
    // descomprimir archivo
    decompress('src/uploads/' + myfileApi + '.zip', 'src/uploads/xApi/' + myfileApi)
    newActivity.user = req.user.id;
    newActivity.idCurse = req.user.idCurse;
    newActivity.fileActivity = myfileApi
    // esta variable guarda los datos que seran enviados por correo electronico
            contentHTML = `
            <h1>Nueva actividad registrada ATHENAS </h1>
            <h3>Nombre de la actividad: ${nameActivity}</h3>
            <h3>Descripción: ${description}</h3>
            <hr>
            <h3>fecha de inicio: ${start_date}</h3>
            <h3>fecha final: ${end_date}</h3>`;

            emailC = `${req.user.email}`;
            // configuración del mi correo electronico
            const transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: 'dracoservivios@gmail.com',
                pass: 'yqjmpfdiiihfuago', // generated ethereal password
            },
            });

            // contenido de correo electronico
            const info = transporter.sendMail({
            from: '"Actividad creada 👍✔" <dracoservivios@gmail.com>', // sender address
            to: emailC, // list of receivers
            subject: "Se a creado nueva actividad en el sistema", // Subject line
            text: "Mensaje de confirmación 👨‍💻", // plain text body
            html: contentHTML, // html body
            });

    await newActivity.save();
    req.flash('success_msg', 'Módulo agregado exitosamente')
    res.redirect('/home');
});

// con esta funcion podemos ver las actividades del usuario relacionadas con la clase
router.get('/activity/:id', isAuthenticated, async (req, res) => {
    const clases2 = await clases.findById(req.params.id)
    await Activity.find({
            idClass: req.params.id
        })
        .sort({
            date: 'desc'
        })
        .then(documentos => {
            const contexto = {
                course: documentos.map(documento => {
                    return {
                        nameActivity: documento.nameActivity,
                        description: documento.description,
                        _id: documento._id,
                        fileActivity: documento.fileActivity
                    }
                })
            }
            res.render('activity/all-activity', {
                course: contexto.course,
                clases2
            })
        })
});

// esta funcion envia la actividad que quiero editar por medio de su id al formulario
router.get('/activity/edit/:id', isAuthenticated, async (req, res) => {
    const Activityedit = await Activity.findById(req.params.id)
    res.render('activity/edit-activity', {
        Activityedit
    })

});

// esta funcion guarda los datos editados
router.put('/activity/edit-activity/:id', isAuthenticated, async (req, res) => {
    const {
        nameActivity,
        description,
        start_date,
        end_date
    } = req.body;
    await Activity.findByIdAndUpdate(req.params.id, {
        nameActivity,
        description,
        start_date,
        end_date
    });
    req.flash('success_msg', 'Módulo editado correctamente')
    res.redirect('/home')
});

// Eliminar nota por parte del docente
router.delete('/activity/delete/:id', isAuthenticated, async (req, res) => {
    await Activity.findByIdAndDelete(req.params.id);
    req.flash('remove', 'Módulo')
    res.redirect('/home')

});


router.get('/result_activity/', isAuthenticated, async (req, res) => {
    await ActivityXapi.find({
            idCurse: req.user.idCurse
        }).sort({
            date: 'desc'
        })
        .then(documentos => {
            const contexto = {
                xApi: documentos.map(documento => {
                    return {
                        nomPrueba: documento.qt,
                        time: documento.fut,
                        required_score: documento.tp,
                        score_won: documento.sp,
                        email: documento.email
                    }
                })
            }

            res.render('activity/solutionXapi', {
                xApi: contexto.xApi
            })
        })
});

// Función para mostrar el listado de estudiantes en el perfil del docente
router.get('/list_estudent/', isAuthenticated, async (req, res) => {
    await User.find({
            idCurse: req.user.idCurse,
            rol: 'Estudent',
        })
        .then(documentos => {
            const contexto = {
                student: documentos.map(documento => {
                    return {
                        name: documento.name,
                        email: documento.email,
                        mobile: documento.mobile,
                        _id: documento._id,
                        date: documento.date,
                    }
                })
            }

            res.render('unidad/resul_student', {
                student: contexto.student
            })
        })
});

// función para ver detalle del las actividades realizadad por el estudiante seleccionaldo
router.get('/follow/:id', isAuthenticated, async (req, res) => {
    resultUser = await User.findById(req.params.id)
    await ActivityXapi.find({
            user: req.params.id
        }).sort({
            date: 'desc'
        })
        .then(documentos => {
            const contexto = {
                ActivityXapi: documentos.map(documento => {
                    return {
                        nomPrueba: documento.qt,
                        time: documento.fut,
                        required_score: documento.tp,
                        score_won: documento.sp,
                        email: documento.email
                    }
                })
            }
            res.render('unidad/follow_up_student', {
                resultUser,
                ActivityXapi: contexto.ActivityXapi
            })
        })

});

router.post('/studentSolution/', isAuthenticated, (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Credentials'), true
    res.header('Access-Control-Allow-Methods'), 'POST'
    res.header('Access-Control-Allow-Headers'), 'Content-Type'
    const {
        tp,
        qt,
        Sid,
        Utah,
        fut,
        Dr, //resultados en xml
        psp,
        sp
    } = req.body;

    const xApiResolve = new ActivityXapi({
        tp,
        qt,
        Sid,
        Utah,
        fut,
        Dr, //resultados en xml
        psp,
        sp
    });


    xApiResolve.user = req.user.id;
    xApiResolve.email = req.user.email;
    xApiResolve.idCurse = req.user.idCurse;
    xApiResolve.save();
    res.render('activity/solution-general')

});

// con esta funcion podemos ver las actividades del usuario relacionadas con la clase
router.get('/studentSolution/:id', isAuthenticated, async (req, res) => {
    await solutionActivity.find({
            idActivity: req.params.id
        }).sort({
            date: 'desc'
        })
        .then(documentos => {
            const contexto = {
                course: documentos.map(documento => {
                    return {
                        user: documento.user,
                        email: documento.email,
                        nameActivity: documento.nameActivity,
                        solution_activity: documento.solution_activity,
                        _id: documento._id,
                        fileActivity: documento.fileActivity,
                        idActivity: documento.idActivity,
                        nota: documento.nota,
                        idClass: documento.idClass

                    }
                })
            }
            res.render('activity/solution-activity', {
                course: contexto.course
            })
        })
});

// esta guarda la nota asignada por el docente
router.put('/activity/calification/:id', isAuthenticated, async (req, res) => {
    const {
        nameActivity,
        solution_activity,
        date,
        idActivity,
        nota,
        email,
        user,
        idClass
    } = req.body;
    await solutionActivity.findByIdAndUpdate(req.params.id, {
        nameActivity,
        solution_activity,
        idActivity,
        nota,
        email,
        user,
        idClass
    });
    req.flash('success_msg', 'Actividad editada correctamente')
    res.redirect('/home')

    my_ruta = ['/activity/', idClass]
    console.log(my_ruta)
});

module.exports = router;