const express = require('express');
const router = express.Router();
const User = require('../models/User'); //requerimos el modelo User (Esto para accder a la BD)
const passport = require('passport')
const nodemailer = require("nodemailer");
const clases = require('../models/class');
const Activity = require('../models/Activity');
const Course = require('../models/course');
const {isAuthenticated} = require('../helpers/auth');



// ruta para ingresar a la aplicación
router.get('/users/signin', (req, res) => {
    res.render('users/signin');
});
// ruta para autenticarse
router.get('/users/signup', (req, res) => {
    res.render('users/signup');
});

router.post('/users/signin', passport.authenticate('local' ,{
    successRedirect: '/home',
    failureRedirect: '/users/signin',
    failureFlash: true
}));

/////////////////////////////////////////////////////////////////////

// ruta para recibir los datos de registro de usuario
router.post('/users/signup', async(req, res) => {
    const { name, email, password, confirm_password} = req.body;
    var rol = 'Admin';
    const errors = []
    // mensaje de error en caso de que no escriba nombre de usario
    if (name.length <= 0) {
        errors.push({ text: 'Ingrese un nombre de usario' })
    }
    // mensaje de error en caso de que no escriba email
    if (email.length <= 0) {
        errors.push({ text: 'Debe ingresar un correo' })
    }
    // mensaje de error en caso de que el campo de contraseña es vacio
    if (password.length <= 0) {
        errors.push({ text: 'Debe ingresar una contraseña' })
    }

    // mensaje de error en caso de las contraseñas no coincidan
    if (password != confirm_password) {
        errors.push({ text: 'Las contraseñas no coinciden' })
    }
    // mensaje de error en caso de que la contraseñas tengan una longitud menor a 4 caracteres
    if (password.length < 4) {
        errors.push({ text: 'Debe ingresar una contraseña con mas de 4 caracteres' })
    }
    if (errors.length > 0) {
        res.render('users/signup', {
            errors,
            name,
            email,
            password,
            confirm_password
        });
    } else {
        // validar si el email exite en la BD
        const emailUser = await User.findOne({email: email})
        if(emailUser){
            req.flash('success_msg', 'Ya existe un usuario registrado con el Correo Ingresado')
            res.redirect('/users/signup')
        }else{
        // Guardar un dato nuevo (agregar datos a la BD)
        const newUser = new User({name, email, password, rol})
        newUser.password = await newUser.encryptPassword(password)
        await newUser.save();
        req.flash('success_msg', 'registro exitoso')
        res.redirect('/users/signin')
        }
    }
});

router.get('/users/logout', function(req, res){
    req.logout();
    res.redirect('/');
  });

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// functiones para editar y eliminar usuarios

// esta funcion guarda los datos editados
router.put('/user/edit-user/:id', isAuthenticated, async (req, res) => {
    const { name, email, mobile } = req.body;
    await User.findByIdAndUpdate(req.params.id, { name, email, mobile });
    req.flash('success_msg', 'El usuario fue editado')
    res.redirect('/home')
});

// esta funcion elimina los usuarios
router.delete('/user/delete/:id', isAuthenticated, async (req, res) => {
    await User.findByIdAndDelete(req.params.id);
    req.flash('remove', 'Usuario')
    res.redirect('/home')
 
});


  // RUTAS PRINCIPALES PARA EL INGRESO A LA PLATAFORMA SER REDIRECCIONA LA PAGINA SEGUN SEA NECESARIO
router.get('/home', isAuthenticated, async (req, res) => {
    if(req.user.rol == 'Admin'){
        // esta variable guarda los datos que seran enviados por correo electronico
        contentHTML = `
        <h1>Ingreso a la plataforma ✔👍 </h1>
        <h3>Usuario 👩‍💼: ${req.user.email}</h3>
        <h3>Rol: ${req.user.rol}</h3>`;

        emailUser = `${req.user.email}`;
        // configuración del mi correo electronico
        const transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
            user: 'dracoservivios@gmail.com',
            pass: 'yqjmpfdiiihfuago', // generated ethereal password
            },
        });

        // contenido de correo electronico
        // const info =  transporter.sendMail({
        //     from: '"  INICIO DE SESIÓN 👍✔" <dracoservivios@gmail.com>', // sender address
        //     to: emailUser, // list of receivers
        //     subject: "Ingreso al sistema ATHENAS", // Subject line
        //     text: "Mensaje de confirmación 👨‍💻", // plain text body
        //     html: contentHTML, // html body
        //   });

        await Course.find(
            {user: req.user.id}).sort({ date: 'desc' })
        .then(documentos => {
            const contexto = {
                course: documentos.map(documento => {
                    return {
                        nameCourse: documento.nameCourse,
                        description: documento.description,
                        _id: documento._id,
                        date: documento.date
                    }
                })
            }

            res.render('course/all-course', {
                course: contexto.course
            })
        })
    }
    else
    if(req.user.rol == 'Teacher'){
        course = await  Course.findOne({_id: req.user.idCurse})
        // funcion que muestra el home del docente o manager 
        await clases.find({
            user: req.user.id
        }).sort({
            date: 'desc'
        })
        .then(documentos => {
            const contexto = {
                module: documentos.map(documento => {
                    return {
                        nameClass: documento.nameClass,
                        description: documento.description,
                        _id: documento._id,
                        date: documento.date
                    }
                })
            }
            res.render('unidad/all-unidad', {
                module: contexto.module,
                course
            })
        }) 
    }else
    if(req.user.rol == 'Estudent'){
        // funcion para mostrar las notificationes en el sistema
        consultadb = await  clases.findOne({idCurse: req.user.idCurse}).sort({$natural:-1}).limit(1);
        consultadb2 = await  Activity.findOne({idCurse: req.user.idCurse}).sort({$natural:-1}).limit(1);
        notification = []
        notas_notification = (["Resolver",
                            consultadb.nameClass]) 
    
        notification2 = consultadb.nameClass
        if(consultadb2 != null){
            notification3 = consultadb2.nameActivity
        }else{
            notification3 = ""
        }
        notification.push(
           { notification: notas_notification}
           )
    
        // Función que muesta el home del estudiante - en este caso se muestra el curso al cual se encuentra matriculado
        await Course.find(
            {_id: req.user.idCurse}).sort({ date: 'desc' })
        .then(documentos => {
            const contexto = {
                course: documentos.map(documento => {
                    return {
                        nameCourse: documento.nameCourse,
                        general_objective: documento.general_objective,
                        specific_goal: documento.specific_goal,
                        problem_question: documento.problem_question,
                        Competencies: documento.Competencies,
                        results: documento.results,


                        _id: documento._id,
                        date: documento.date,
                        userAdmin: documento.user,
                        useremail: req.user.email,
                        user: req.user._id,
                        
                    }
                })
            }
            res.render('student/home-student', {
                course: contexto.course,
                consultadb,
                notification,
                notification2,
                notification3
            })

        })
    } 
});


// ver datos especificos de mi cuenta
router.get('/my_account', isAuthenticated, async (req, res) =>{
    const user = await User.findById(req.user._id)
    res.render('users/my_account', { user })
  });

  // esta funcion guarda los datos editados
router.put('/user/edit_Myaccount/:id', isAuthenticated, async (req, res) => {
    const { name, mobile } = req.body;
    await User.findByIdAndUpdate(req.params.id, { name, mobile });
    req.flash('success_msg', 'Datos de usuario editados')
    res.redirect('/my_account')
});


// RUTA PARA VISUALIZAR TODOS LOS USUARIOS
router.get('/list_user/:id', isAuthenticated, async (req, res) => {
    await User.find({
        user: req.params.id, 
        }).sort({
             date: 'desc'
             })
        .then(documentos => {
            const contexto = {
                teacher: documentos.map(documento => {
                    return {
                        name: documento.name,
                        email: documento.email,
                        mobile: documento.mobile,
                        _id: documento._id,
                        date: documento.date, 
                    }
                })
            }
        res.render('users/all_user', {
            teacher: contexto.teacher
        })
    })
});


module.exports = router;