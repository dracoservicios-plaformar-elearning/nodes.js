const express = require('express');
const router = express.Router();

// constate que llama al modelo de la bd
const Note = require('../models/Note');

// funcion para restringir el acceso por usuario
const {isAuthenticated} = require('../helpers/auth');


router.get('/notes/add', isAuthenticated, (req, res) => {
    res.render('notes/new-note');
});

// ruta para recibir datos INGRESADOS POR EL USUARIO
router.post('/notes/new-note', isAuthenticated ,async (req, res) => {
    const { title, description } = req.body;
    const errors = [];
    // si no se encientra el titulo sale msj
    if (!title) {
        errors.push({ text: 'por favor escriba el titulo' })
    }
    // si no se encuentra una descripcion sale error
    if (!description) {
        errors.push({ text: 'por favor escriba una descripcion' })
    }
    // si los errores son mayor a 0
    if (errors.length > 0) {
        res.render('notes/new-note', {
            errors,
            title,
            description
        });
    } else {
        // SI TODO SALE BIEN Y SE RECIEBEN LOS DATOS INGRESADOS Y SE GUARDA EN LA BD
        const newNote = new Note({ title, description });
        // console.log(newNote);
        newNote.user = req.user.id;
        await newNote.save();
        req.flash('success_msg', 'Nota agregada exitosamente')
        res.redirect('/notes');

    }
});
// ruta ver las notas registradas
//Funcion para llamar las notas de la bd y redireccionarlas para mostrarlas
router.get('/notes', isAuthenticated, async (req, res) => {
    await Note.find({user: req.user.id}).sort({ date: 'desc' })
        .then(documentos => {
            const contexto = {
                notes: documentos.map(documento => {
                    return {
                        title: documento.title,
                        description: documento.description,
                        _id: documento._id,
                        date: documento.date,
                        useremail: req.user.email
                    }
                })
            }
            res.render('notes/all-notes', {
                notes: contexto.notes
            })
        })
});
// esta funcion envia la nota que quiero editar por medio de su id al formulario
router.get('/notes/edit/:id', isAuthenticated,async (req, res) => {
    const note = await Note.findById(req.params.id)
    res.render('notes/edit-note', { note })
    // console.log(note.title)
    // console.log(note.description)
});

// esta funcion guarda los datos editados
router.put('/notes/edit-note/:id', isAuthenticated, async (req, res) => {
    const { title, description } = req.body;
    await Note.findByIdAndUpdate(req.params.id, { title, description });
    req.flash('success_msg', 'Nota editada correctamente')
    res.redirect('/notes')
});

// esta funcion elimina la nota
router.delete('/notes/delete/:id', isAuthenticated, async (req, res) => {
    await Note.findByIdAndDelete(req.params.id);
    req.flash('remove', 'Nota eliminada')
    res.redirect('/notes')
 
});

module.exports = router;
