const express = require('express');
const router = express.Router();
// constate que llama al modelo de la bd
const Course = require('../models/course');
// funcion para restringir el acceso por usuario
const {isAuthenticated} = require('../helpers/auth');
const ActivityXapi = require('../models/ActivityXapi');
const Activity = require('../models/Activity');
const unidad = require('../models/class');


// ruta para agregar nuevo curso
router.get('/course/add', isAuthenticated, (req, res) => {
    res.render('course/new-course');
});
// ruta para recibir datos INGRESADOS POR EL USUARIO - TENER EN CUENTA QUE LOS RESIVE POR EL METODO POST
router.post('/course/new-course', isAuthenticated ,async (req, res) => {
    const { nameCourse, specific_goal, general_objective, problem_question, Competencies, results} = req.body;
    const errors = [];
    // si no se encientra el titulo sale msj
    if (!nameCourse) {
        errors.push({ text: 'Debe ingresar el nombre del curso' })
    }
    // si no se encuentra una descripcion sale error
    if (!specific_goal) {
        errors.push({ text: 'por favor escriba una descripcion' })
    }
    if (!general_objective) {
        errors.push({
            text: 'Debe escribir el Objetivo General'
        })
    }
    if (!problem_question) {
        errors.push({
            text: 'Debe escribir la pregunta problema'
        })
    }
    if (!Competencies) {
        errors.push({
            text: 'Debe escribir las competencias'
        })
    }
    if (!results) {
        errors.push({
            text: 'Debe escribir los resultados esperado'
        })
    }
    // si los errores son mayor a 0
    if (errors.length > 0) {
        res.render('course/new-course', {
            errors,
            nameCourse,
            specific_goal,
            general_objective,
            problem_question,
            Competencies, 
            results
        });
    } else {
        // SI TODO SALE BIEN Y SE RECIEBEN LOS DATOS INGRESADOS Y SE GUARDA EN LA BD
        const newCourse = new Course({ nameCourse, 
            specific_goal,  
            general_objective,
            problem_question,
            Competencies, 
            results });
        //console.log(newCourse);
        newCourse.user = req.user.id;
        await newCourse.save();
        req.flash('success_msg', 'Se registro un nuevo curso')
        res.redirect('/course');


    }
});

// RUTA PARA VISUALIZAR LOS CURSOS CREADOS
router.get('/course', isAuthenticated, async (req, res) => {
    await Course.find({user: req.user.id}).sort({ date: 'desc' })
        .then(documentos => {
            const contexto = {
                course: documentos.map(documento => {
                    return {
                        nameCourse: documento.nameCourse,
                        description: documento.description,
                        _id: documento._id,
                        date: documento.date
                    }
                })
            }
            res.render('course/all-course', {
                course: contexto.course
            })
        })
});

// esta funcion envia la nota que quiero editar por medio de su id al formulario
router.get('/course/edit/:id', isAuthenticated,async (req, res) => {
    const course = await Course.findById(req.params.id)
    res.render('course/edit-course', { course })
 
});

// esta funcion guarda los datos editados
router.put('/course/edit-course/:id', isAuthenticated, async (req, res) => {
    const { nameCourse, 
        general_objective,
        specific_goal,
        problem_question,
        Competencies,
        results
     } = req.body;
    await Course.findByIdAndUpdate(req.params.id, { nameCourse, 
        general_objective,
        specific_goal,
        problem_question,
        Competencies,
        results });
    req.flash('success_msg', 'Nota editada correctamente')
    res.redirect('/course')
});

// esta función permite ver detalles del curso
router.get('/detail/:id', isAuthenticated, async (req, res) => {
    const detailcourse = await Course.findById(req.params.id)
    res.render('course/detail-course', {
        detailcourse
    })

});
// esta funcion elimina el curso
router.delete('/course/delete/:id', isAuthenticated, async (req, res) => {
    await Course.findByIdAndDelete(req.params.id);
    req.flash('remove', 'Curso')
    res.redirect('/course')
 
});

// RUTA PARA VIZUALIAZAR TODOS LOS CURSOS
router.get('/list_course/:id', isAuthenticated, async (req, res) => {
    await Course.find({
        user: req.params.id, 
        }).sort({
             date: 'desc'
             })
        .then(documentos => {
            const contexto = {
                course: documentos.map(documento => {
                    return {
                        _idCourse: documento._id,
                        nameCourse: documento.nameCourse,
                        general_objective: documento.general_objective,
                        problem_question: documento.problem_question,
                        Competencies: documento.Competencies, 
                    }
                })
            }
        res.render('course/all-general', {
            course: contexto.course
        })
    })
});
// RESULTADO GENERAL
router.get('/result_general/:id', isAuthenticated, async (req, res) => {
    title= (['Resultados Generales']),
    idCourse = ('nada')

    await ActivityXapi.find({
        // user: req.params.id, 00000 filtro admin
        }).sort({
             date: 'desc'
             })
        .then(documentos => {
            const contexto = {
                Activity: documentos.map(documento => {
                    return {
                        nomPrueba: documento.qt,
                        time: documento.fut,
                        required_score: documento.tp,
                        score_won: documento.sp,
                        email: documento.email,
                        _id:  documento._id
                    }
                })
            }
        res.render('course/all-result', {
            Activity: contexto.Activity,
            title,
            idCourse
            
        })
    })
});

// RESULTADO ESPECIFICOS POR CURSO
router.get('/result_specific/:id', isAuthenticated, async (req, res) => {
    title= (['Resultados Especificos'])
    idCourse = req.params.id
    await ActivityXapi.find({
        idCurse: req.params.id,
        }).sort({
             date: 'desc'
             })
        .then(documentos => {
            const contexto = {
                Activity: documentos.map(documento => {
                    return {
                        nomPrueba: documento.qt,
                        time: documento.fut,
                        required_score: documento.tp,
                        score_won: documento.sp,
                        email: documento.email,
                        _id:  documento._id
                    }
                })
            }
        res.render('course/all-result', {
            Activity: contexto.Activity,
            title,
            idCourse
        })
    })
});

// listar unidades 
router.get('/list_unidades/:id', isAuthenticated, async (req, res) => {
    await unidad.find({
        user: req.params.id, 
        }).sort({
             date: 'desc'
             })
        .then(documentos => {
            const contexto = {
                unidad: documentos.map(documento => {
                    return {
                        nameClass: documento.nameClass,
                        general_objective: documento.general_objective,
                        specific_goal: documento.tp,
                        date: documento.date,
                        _id:  documento._id
                    }
                })
            }
        res.render('unidad/list-unidad', {
            unidad: contexto.unidad
        })
    })
});

// resetear curso
router.delete('/delete_result/:id', isAuthenticated, async (req, res) => {
    await ActivityXapi.deleteMany({idCurse: req.params.id});
    req.flash('remove', 'Resultados')
    res.redirect('/home')
 
});

//elimar unidad
router.delete('/unidad/delete/:id', isAuthenticated, async (req, res) => {
    await unidad.findByIdAndDelete(req.params.id);
    await Activity.deleteMany({idClass: req.params.id});
    req.flash('remove', 'Unidad')
    res.redirect('/home')
 
});


module.exports = router;

