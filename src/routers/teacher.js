const express = require('express');
const router = express.Router();
const User = require('../models/User'); //requerimos el modelo User (Esto para accder a la BD)
const nodemailer = require("nodemailer");
const Course = require('../models/course');
const {isAuthenticated} = require('../helpers/auth');


// esta funcion envia al formulario para crear docente, con la caracterisca que lleva el id del curso
router.get('/teacher/add/:id', isAuthenticated, async(req, res) => {
    const createTeacher = await Course.findById(req.params.id)
    res.render('teacher/new-teacher', { createTeacher })
});

// ruta para recibir los datos de registro de docente
router.post('/teacher/new-teacher',isAuthenticated, async(req, res) => {
    const { name, email, mobile, password, confirm_password, idCurse} = req.body;
    var rol = 'Teacher';
    const errors = []
    //console.log(req.body);
    // mensaje de error en caso de que no escriba nombre de usario
    if (name.length <= 0) {
        errors.push({ text: 'Ingrese un nombre de usario' })
    }
    // mensaje de error en caso de que no escriba email
    if (email.length <= 0) {
        errors.push({ text: 'Debe ingresar un correo' })
    }
    // mensaje de error en caso de que el campo de contraseña es vacio
    if (password.length <= 0) {
        errors.push({ text: 'Debe ingresar una contraseña' })
    }

    // mensaje de error en caso de las contraseñas no coincidan
    if (password != confirm_password) {
        errors.push({ text: 'Las contraseñas no coinciden' })
    }
    // mensaje de error en caso de que la contraseñas tengan una longitud menor a 4 caracteres
    if (password.length < 4) {
        errors.push({ text: 'Debe ingresar una contraseña con mas de 4 caracteres' })
    }

    if (errors.length > 0) {
        res.render('teacher/new-teacher', {
            errors,
            name,
            email,
            mobile,
            password,
            confirm_password
        });
    }else {
        // validar si el email exite en la BD
        const emailUser = await User.findOne({email: email})
        if(emailUser){
            req.flash('success_msg', 'Ya existe un usuario registrado con el Correo Ingresado')
            res.redirect('course')
        }else{
        
              // esta variable guarda los datos que seran enviados por correo electronico
        contentHTML = `
        <h1>Registro de usuario ✔👍 </h1>
        <h3>Nombre: ${name}</h3>
        <h3>Usuario: ${email}</h3>
        <h3>Contraseña: ${password}</h3>
        <h3>Rol: ${rol}</h3>`;


        emailC = `${email}`;
        // configuración del mi correo electronico
        const transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
            user: 'dracoservivios@gmail.com',
            pass: 'yqjmpfdiiihfuago', // generated ethereal password
            },
        });

        // contenido de correo electronico
        const info =  transporter.sendMail({
            from: '"Registro de docente 👍✔" <dracoservivios@gmail.com>', // sender address
            to: emailC, // list of receivers
            subject: "Acaba de registrar un nuevo usuario al sistema", // Subject line
            text: "Mensaje de confirmación 👨‍💻", // plain text body
            html: contentHTML, // html body
          });

        // Guardar un dato nuevo (agregar datos a la BD)
        const newUser = new User({name, email, mobile, password, rol, idCurse})
        newUser.password = await newUser.encryptPassword(password)
        ///newTeacher.course = req.course.id;
        newUser.user = req.user.id;//id del adminstrador 
        await newUser.save();

        req.flash('success_msg', 'registro exitoso')
        res.redirect('../home');
        }

    }
});

// RUTA PARA VISUALIZAR LOS DOCENTES
router.get('/teacher/:id', isAuthenticated, async (req, res) => {
        await User.find({
            idCurse: req.params.id, rol: 'Teacher', 
            }).sort({
                 date: 'desc'
                 })
            .then(documentos => {
                const contexto = {
                    teacher: documentos.map(documento => {
                        return {
                            name: documento.name,
                            email: documento.email,
                            mobile: documento.mobile,
                            _id: documento._id,
                            date: documento.date, 
                        }
                    })
                }
            res.render('teacher/all-teacher', {
                teacher: contexto.teacher
            })
        })
});


// esta funcion envia la docente que quiero editar por medio de su id al formulario
router.get('/teacher/edit/:id', isAuthenticated,async (req, res) => {
    const teacher = await User.findById(req.params.id)
    res.render('teacher/edit-teacher', { teacher })
});





module.exports = router;