const express = require('express');
const path = require('path');
const Handlebars = require('handlebars')
const expressHandlebars = require('express-handlebars')
const methodOverride = require('method-override')
const session = require('express-session')
const { allowInsecurePrototypeAccess } = require('@handlebars/allow-prototype-access')
const flash = require('connect-flash')
const passport = require('passport')

const app = express();
require('./database') //requiero archivo de la BD
require('./config/passport') //llamamos la configuración de la autenticación

//settings 
app.set('port', process.env.PORT || 3000)
app.set('views', path.join(__dirname, 'views'))

app.engine('.hbs', expressHandlebars({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs',
    handlebars: allowInsecurePrototypeAccess(Handlebars),
    helpers: {
        equal: function (u,d){
            return u == d? true : false;
        },   
    }
}));
app.set('view engine', '.hbs');

//Middlewares
app.use(express.urlencoded({ extended: false }))//esto es usado para entender los datos que envia el formulario
app.use(express.json()); //se entenderan los datos en formato json
app.use(methodOverride('_method'))//sirve para los formularios puedan enviar otro tipo de datos como PUT
app.use(session({
    secret: 'mysecretapp',
    resave: true,
    saveUninitialized: true
}));


app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

//Global Variables
app.use((req, res, next) => {

    res.locals.success_msg = req.flash('success_msg');
    res.locals.remove = req.flash('remove');
    res.locals.notification = req.flash('notification');
    res.locals.error = req.flash('error');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.user = req.user || null;
    res.locals.notification2 = 'notificación 1'
    res.locals.notification3 = 'notifciación 2'


    next();  
});

//Routers
app.use(require('./routers/index'));
app.use(require("./routers/notas"));
app.use(require("./routers/users"));
app.use(require("./routers/course"));
app.use(require("./routers/teacher"));
app.use(require("./routers/class"));
app.use(require("./routers/student"));
// controller


app.use(express.static(path.join(__dirname, 'public'))) //funcion que nos indica que la carpeta puede ser accedida desde cualquier punto
app.use(express.static(path.join(__dirname, 'uploads')))

//Server is listenning
app.listen(app.get('port')), () => {
    console.log('server on port', app.get('port'));
}
