// Modelo de datos del estudiante
const mongoose = require('mongoose');
const bcryptjs = require('bcryptjs')
const { Schema } = mongoose;

// esquema student
const StudentSchema = new Schema({
    nameStudent: {type: String, require: true},
    email: {type: String, require: true},
    password: {type: String, require: true},
    date: {type: Date, default: Date.now},
    rol: {type: String, require: true},
    // el estudiante es asignado matriculado en un curso
    idCurse: {type: String},
    user: {type: String}

});


// metodo para cifrar contraseña
StudentSchema.methods.encryptPassword = async(password) => {
    const salt = await bcryptjs.genSalt(10);
    const hast = bcryptjs.hash(password, salt);
    return hast;
};

// comparar contraseña ingresada por el usaurio
StudentSchema.methods.matchPassword = async function(password){
    return await bcryptjs.compare(password, this.password)
};

module.exports = mongoose.model('Student',StudentSchema)
 


    

    

