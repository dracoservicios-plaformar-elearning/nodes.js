// esquema de datos para Registrar los resulados de la prueba
const mongoose = require('mongoose');
const { Schema } = mongoose;

const SolutionSchema = new Schema({
    nameActivity: {type: String, require: true},
    solution_activity: {type: String, require: true},
    date: {type: Date, default: Date.now},
    // la ACTIVIDAD es creada por el docente - asignandola a una clase - se crea relacion 
    idClass: {type: String},
    idActivity: {type: String},
    email: {type: String},
    nota: {type: String},
    user: {type: String},
});

module.exports = mongoose.model('Solution',SolutionSchema)


    

    

