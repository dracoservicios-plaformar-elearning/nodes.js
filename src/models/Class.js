// esquema de datos para la creación de un modulo de aprendizaje

const mongoose = require('mongoose');
const { Schema } = mongoose;

const Class_Schema = new Schema({
    nameClass: {type: String, require: true},
    general_objective: {type: String, require: true},
    specific_goal: {type: String, require: true},
    problem_question: {type: String, require: true},
    Competencies: {type: String, require: true},
    results: {type: String, require: true},

    date: {type: Date, default: Date.now},
    // la clase es creada por el docente - se crea relación
    idCurse: {type: String},
    user: {type: String},

});

module.exports = mongoose.model('Clases',Class_Schema)


    

    

