// esquema de datos para la creación de una Actividas

const mongoose = require('mongoose');
const { Schema } = mongoose;

const ActivitySchema = new Schema({
    nameActivity: {type: String, require: true},
    description: {type: String, require: true},
    fileActivity: {type: String, require: true},

    start_date: {type: Date, default: Date()},
    end_date: {type: Date, default: Date()},

   // la ACTIVIDAD es creada por el docente - asignandola a una clase - se crea relacion 
   idCurse: {type: String}, 
   idClass: {type: String, require: true},
   user: {type: String},
   date: {type: Date, default: Date.now}


  
});

module.exports = mongoose.model('Activity',ActivitySchema)


    

    

