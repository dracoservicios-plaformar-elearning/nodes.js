const mongoose = require('mongoose');
const bcryptjs = require('bcryptjs')
const { Schema } = mongoose;
// esquema de datos del usario
const UserSchema = new Schema({
    name: {type: String, require: true},
    email: {type: String, require: true},
    password: {type: String, require: true},
    date: {type: Date, default: Date.now},
    mobile: {type: String, require: true},
    rol: {type: String, require: true},

    idCurse: {type: String},
    user: {type: String}
});

// metodo para cifrar contraseña
UserSchema.methods.encryptPassword = async(password) => {
    const salt = await bcryptjs.genSalt(10);
    const hast = bcryptjs.hash(password, salt);
    return hast;
};

// comparar contraseña ingresada por el usaurio
UserSchema.methods.matchPassword = async function(password){
    return await bcryptjs.compare(password, this.password)
};

module.exports = mongoose.model('User',UserSchema)
 


    

    

