// esquema de datos para la creación de un curso

const mongoose = require('mongoose');
const { Schema } = mongoose;

const CourseSchema = new Schema({
    nameCourse: {type: String, require: true},
    date: {type: Date, default: Date.now},
    general_objective: {type: String, require: true},
    specific_goal: {type: String, require: true},
    problem_question: {type: String, require: true},
    Competencies: {type: String, require: true},
    results: {type: String, require: true},
    // el curso es creado por el administrador - se crea la realcion
    user: {type: String}
});

module.exports = mongoose.model('Course',CourseSchema)


    

    

